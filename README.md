# DESAFIO TYPESCRIPT

# Questões Teóricas

## 1. O que exatamente é o Typescript?

O TypeScript é um pré-processador (superset) de códigos JavaScript open source desenvolvido e mantido pela Microsoft. A sua primeira versão, a 0.8, foi lançada em 1 de outubro de 2012. Ele foi projetado para auxiliar no desenvolvimento de códigos simples até os mais complexos, utilizando os princípios de Orientação a Objetos, como classes, tipagens, interfaces, Generics etc. Embora o Typescript seja um superset do Javascript, na hora de compilar o código, todo Typescript é convertido/transpilado para Javascript.

## 2. Descreva os tipos básicos do Typescript.

**Boolean:** O Boolean suporta dois tipos de valores: true ou false.

~~~javascript
let isPresent:boolean = true;
~~~

**Number:** No TypeScript, todos os valores numéricos, como floating , decimal , hex , octal devem ser tipados como number.

~~~javascript
let octal: number = 0o745;
let binary: number = 0b1111;
let decimal: number = 34;
let hex: number = 0xf34d;
~~~

**String:** É usado para armazenar dados de texto. Os valores de string são colocados entre aspas simples ou aspas duplas.

~~~javascript
let cor: string = "verde"; 
cor = 'azul';
~~~

**Array:** É um tipo de dado que pode armazenar vários valores de diferentes tipos de dados sequencialmente. Nós declaramos um array no TypeScript utilizando as chaves [ ] .

~~~javascript
let numeros: number[ ] = [1, 2, 3]; 
~~~

Ou nós podemos utilizar a palavra reservada Array< > : 

~~~javascript
let numeros: Array<number> = [1, 2, 3]; 
~~~

**Tuple:** As tuplas, representam uma estrutura de dados simples semelhante a um array. A grande diferença entre eles é que nos arrays nós trabalhamos somente com um tipo de dado, enquanto com as tuplas temos diferentes tipos. 

~~~javascript
let list: [string, number, string];
let list = ['Filipe Cunha', 19 , 'filipecunha@gmail.com'];
~~~

**Enum:** O enum nos permite declarar um conjunto de valores/constantes predefinidos. 

Existem três formas de se trabalhar com ele no TypeScript: 

**-> Number:** Os enums numéricos armazenam strings com valores numéricos

~~~javascript
export enum DiaDaSemana {
    Segunda = 1,
    Terca = 2,
    Quarta = 3,
    Quinta = 4,
    Sexta = 5,
    Sabado = 6,
    Domingo = 7
}
~~~

Caso você não passe um valor inicial na declaração do seu enum , o compilador do TypeScript fará um autoincremento de +1 iniciando em 0 até o último elemento do enum .

**-> String:** Os enums do tipo string precisam iniciar com um valor. 

~~~javascript
export enum DiaDaSemana {
    Segunda = "Segunda-feira",
    Terca = "Terça-feira",
    Quarta = "Quarta-feira",
    Quinta = "Quinta-feira",
    Sexta = "Sexta-feira",
    Sabado = "Sábado",
    Domingo = "Domingo",
}
~~~

**-> Heterogeneous:** Eles aceitam os dois tipos de valores: string e number.

~~~javascript
export enum Heterogeneous {
    Segunda = 'Segunda-feira',
    Terca = 1,
    Quarta,
    Quinta,
    Sexta,
    Sabado,
    Domingo = 18,
}
~~~

**Any:** É um tipo que pode ser modificado para qualquer outro tipo presente na linguagem. 
Quando não conhecemos 100% a estrutura de outro projeto, como uma API por exemplo (esse é um dos cenários em que o any é mais utilizado).

~~~javascript
let variavelAny: any = "Variável";
variavelAny = 34;
variavelAny = true;
~~~

**Void:** O type void é bastante utilizado em funções. Diferente do type any , que espera o retorno de qualquer valor, o void passa para o compilador que aquela função não terá nenhum retorno. 

~~~javascript
function olá( ): void { 
    console.log('Olá!');
}
~~~

**Null:** Indica ausência de um objeto.

~~~javascript
if (b === 0) {  
    return {  
      error: 'Division by zero',  
      result: null  
    };
}
~~~

**Undefined:**  Indica a ausência de qualquer valor.

~~~javascript
if (ratio === undefined) {  
  console.log('Someone forgot to assign a value.'); 
}
~~~

**O TypeScript Coding guidelines recomendam o uso apenas de valores undefineds e desencorajam o uso de valores nulls.**

**Never:** O tipo never é usado quando você tem certeza de que algo nunca irá ocorrer. Por exemplo, você escreve uma função que não retornará ao seu ponto final ou sempre lançará uma exceção. O tipo never nunca pode receber um valor.

~~~javascript
function keepProcessing(): never {
    while (true) {
        console.log('I always does something and never ends.')
    }
}
~~~

**Union:** Nos permite combinar um ou mais tipos. Sua sintaxe é um pouco diferente dos outros tipos, ele utiliza uma barra vertical para passar os tipos que ele deve aceitar.

~~~javascript
let code: (string | number);
code = 123;   // OK
code = "ABC"; // OK
code = false; // Compiler Error
~~~

**Object:** O tipo de objeto no TypeScript representa valores não primitivos como uma matriz, função ou objeto JavaScript simples. E excluem todos os valores que são strings, numbers, booleans, symbols, null, or undefined.

~~~javascript
let data: object;

data = { key: "value" }; // OK
data = [1, 2, 3]; // OK
data = () => {}; // OK
data = "string"; // Error
data = 42; // Error
data = true; // Error
data = null; // Error
data = undefined; // Error
~~~

## 3. O que é uma interface?

A Interface é uma sintaxe do TypeScript que define as especificações de uma entidade. Ou seja, define o contrato, a estrutura de uma Classe, Objeto ou Função. No TypeScript, uma interface contém apenas a definição de métodos e propriedades, não sua implementação. Uma interface é definida usando a keyword “interface”, seguida do nome e das especificações. Por exemplo:

~~~javascript
interface User {
  name: string;
  pass: string;
  email: string;
}
~~~

## 4. O que é uma classe?

Uma classe é um molde com o qual os objetos são modelados. É nela que passamos quais atributos um objeto deve ter e quais ações ele deve executar. 

// Exemplo de implementação de uma classe:

~~~javascript
class Pessoa {
	nome: string;
	idade: number;
	estaVivo: boolean;
	
	constructor(nome: string, idade: number, estaVivo: boolean){
		this.nome = nome;
		this.idade = idade;
		this.estaVivo = estaVivo;
	}
}

let pessoa = new Pessoa('Paulo', 0, true);
~~~

## 5. O que é a herança?

Um dos pilares da POO é a herança, ela nos permite reutilizar código sem a necessidade de duplicá-lo. Uma classe filha pode herdar características e comportamentos da classe pai sem que seja preciso redefinir as funções.

Para definirmos que uma Classe herda de outra, utilizamos a palavra reservada extends, informando qual Classe está sendo herdada.

~~~javascript
class Animal {}

class Dog extends Animal {}
~~~

## 6. Quais as vantagens de usar o Typescript ao invés de somente o Javascript?

* Tipagem estática: A principal caraterística do TypeScript é sua tipagem forte. A utilização de tipagem ajuda no momento de depuração (debug) do nosso código, prevenindo alguns possíveis bugs ainda em tempo de desenvolvimento.
* Orientação a Objetos.
* Feedback mais rápido de erros.
* Processo de refatoração mais fácil.
* Autocomplete da linguagem, muito boa no vscode.
* Poder adotar gradualmente typescript em uma base de código.

## Instruções
Para executar o projeto, primeiro rode o comando: ```npm i``` para baixar as dependências do projeto.

A função `uuidv4` serve apenas para gerar uma string que será usada como o id quando necessário.

Para compilar e rodar o projeto você primeiro deve executar o comando `npx tsc` para transformar os arquivos `.ts` em `.js`.
Depois disso, os arquivos compilados estarão na pasta `dist`. Para executar, basta executar o seguinte comando: `node ./dist/index.js`.

# Questões práticas
- [ ] Agora que você é bem familiarizado com o Typescript, vamos criar um simples projeto: <br>
Primeiro, clone o repositório anexado em sua máquina;
Como deve ter notado, na pasta `src` temos uma pasta `model` e um arquivo `index.js`. Por enquanto apenas guarde essas informações;

- [ ] Comece pelo arquivo `users.js` na pasta `models`. Nele, você deverá adicionar tipagem para as funções e também para o user. Antes de mais nada renomeie o arquivo para `users.ts`. Um user possui os seguintes atributos: `id`, `name`, `age` e `email`. Use de seu conhecimento na criação de interfaces para resolver esta questão;
  
- [ ] Agora, indo para o arquivo `bankAccount.js` na pasta `models`, você deverá adicionar tipagem para as funções e também para a bankAccount. Como na questão anterior, renomeie o arquivo para `bankAccount.ts`. Nessa questão você deverá identificar quais são os atributos da bankAccount de acordo com o que há disponível nas funções. De novo, use seus conhecimentos sobre interfaces e typescript para resolver a questão.

- [ ] Após ter feito a tipagem dos arquivos da pasta `models` é hora de resolver os erros do arquivo `index.js`. Comece trocando sua extensão para `index.ts`. Você logo verá que alguns erros estão acontecendo. Antes não seria possível identificá-los, pois nenhum dos arquivos possuía tipagem. O que você precisa fazer agora é basicamente corrigir os erros de acordo com o que o intellisense do seu editor de códigos indicar. 

- [ ] Com os erros resolvidos, precisamos apenas testar o código e executar a função de `withdraw` e a função de `deposit` da `bankAccount`. Para isso, primeiro deposite a quantia de 1000 unidades monetárias (um) e faça um saque de 200um. Para verificar se deu tudo certo, use a função `getBankAccountInfo` e dê um `console.log` na conta obtida.

## Desafio Extra
- [ ] Refatore o projeto, mas usando classes para criar os `users` e as `bankAccounts`;