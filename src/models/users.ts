import { v4 as uuidv4 } from 'uuid';

export interface User {
  id: string;
  name: string;
  age: number;
  email: string;
}

const users: Array<User> = [];

export function createUser(user:User) {

  user.id = uuidv4();

  users.push(user);
  
  return user; 
}

export function updateUser(user:User):void {
  const index = users.findIndex((item) => item.id === user.id);

  users[index] = user;
}

export function getUserByEmail(email:string) {                     
  return users.find((item) => item.email === email);
}

export function removeUser(id:string):void {                         
  const index = users.findIndex((item) => item.id === id);

  users.splice(index, 1);
}



