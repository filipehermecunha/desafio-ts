import { createBankAccount,deposit,withdraw,getBankAccountInfo } from './models/bankAccount';
import { createUser, updateUser,getUserByEmail } from './models/users';

const user = createUser({ name: 'filipe', age: 20,id:'', email: 'filipehcunha@gmail.com'});

// TODO - pegar usuário criado e passar como parâmetro para a função createBankAccount
const bankAccount = createBankAccount(user, { agency: '0001', number: 1234 });

const accountInfo = getBankAccountInfo( { agency: '0001', number: 1234 });
// TODO - pegar conta criada, realizar um depósito e um saque

const email = getUserByEmail('filipehcunha@gmail.com');

// Depositar 1000um
deposit(bankAccount, 1000);

// Sacar 200um
withdraw(bankAccount, 200);

console.log(accountInfo);
console.log(email);

