"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bankAccount_1 = require("./models/bankAccount");
const users_1 = require("./models/users");
const user = (0, users_1.createUser)({ name: 'John Doe', age: 21, id: '', email: 'filipehcunha@gmail.com' });
// TODO - pegar usuário criado e passar como parâmetro para a função createBankAccount
const bankAccount = (0, bankAccount_1.createBankAccount)(user, { agency: '0001', number: 1234 });
const accountInfo = (0, bankAccount_1.getBankAccountInfo)({ agency: '0001', number: 1234 });
// TODO - pegar conta criada, realizar um depósito e um saque
// Depositar 1000um
(0, bankAccount_1.deposit)(bankAccount, 1000);
// Sacar 200um
(0, bankAccount_1.withdraw)(bankAccount, 200);
console.log(accountInfo);
